# ![logo](https://azgath.com/download/img/AZCC.png) Az'gathCore 8.3.0 

* [Build Status](#build-status)
* [Introduction](#introduction)
* [Requirements](#requirements)
* [Install](#install)
* [Copyright](#copyright)
* [Links](#links)

## Build Status

[![master Build Status](https://api.travis-ci.org/AzgathCore/AzgathCore.svg?branch=master)](https://travis-ci.org/github/AzgathCore/AzgathCore)
[![Build status](https://ci.appveyor.com/api/projects/status/x59wy2wejskf4q0a/branch/master?svg=true)](https://ci.appveyor.com/project/AzgathCore/AzgathCore/branch/master)

## Introduction

Az'gathCore is a *MMORPG* Framework based mostly in C++.

It is derived from *Trinity*, the *Massive Network Game Object Server*, and is
based on the code of that project with extensive changes over time to optimize,
improve and cleanup the codebase at the same time as improving the in-game
mechanics and functionality.

It is completely open source; community involvement is highly encouraged.

If you wish to contribute ideas or code please visit our site linked below or
make pull requests to our [Github repository](https://github.com/AzgathCore/AzgathCore).

For further information on the AzgathCore project, please visit our project
website at [Azgath.com](https://www.azgath.com).

## Requirements

## + Linux :
+ Processor with SSE2 support 
+ Boost ≥ 1.58
+ MySQL ≥ 5.7.0
+ OpenSSL ≥ 1.0.x
+ CMake ≥ 3.13.4
+ Clang  ≥ 5 (heavy recommended, especially on master branch) or GCC ≥ 7.1.0
+ zlib ≥ 1.2.7

## + macOS :
+ Processor with SSE2 support 
+ Boost ≥ 1.60
+ MySQL ≥ 5.7
+ OpenSSL ≥ 1.0.0 
+ CMake ≥ 3.2.0
+ GCC ≥ 6.3.0 or Clang  ≥ 3.3

## + Windows :
+ Processor with SSE2 support
+ Boost ≥ 1.66
+ MySQL ≥ 5.7
+ OpenSSL ≥ 1.0.x
+ CMake ≥ 3.14 (latest stable recommended)
+ MS Visual Studio (Community) ≥ 16.4 (2019) (Desktop) (Not previews)

## Install

Detailed installation guides are available in the [wiki](https://www.trinitycore.info/display/tc/Installation+Guide) for
Windows, Linux and Mac OSX.  
You can get database from  
https://github.com/AzgathCore/AzgathCore/releases

## Copyright

License: GPL 2.0
Read file [COPYING](COPYING)

## Links

* [Website](https://azgath.com/fr/)
* [Discord](https://discord.com/invite/z8Qmhpw)

[![Donate](https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donate_LG.gif "Donate")](https://www.paypal.com/donate/?token=zZVpqY-CsNMU7Esok8kSEwjGDRmH_WzFzeAtwhjhfCnbi32z8fcNLiEd7uJdjZqceTOsKG&country.x=FR&locale.x=FR)
